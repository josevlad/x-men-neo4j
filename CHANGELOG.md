# Changelog
Todo el contenido y los cambios notables en este proyecto serán documentados en este archivo.

El formato se basa en [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
Y este proyecto se adhiere a [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [v1.0.0-rc.1]() - 14 Marzo, 2018
### New

Se ponen a disposicion los siguentes endpoint  

servicios:
  - **post: /api/mutan**  servicio principal para el analisis del los datos (adn). 
  - **get: /api/dna**  Listados de los ADN registrados. 
  - **get: /api/stats**  Estadisticas de los resultados registrados. 
  - **get: /api/matrix/{dim}**  Restorna una muestra aleatoria para analizar 

[1.0.0-rc.1]: http://git-asi.buenosaires.gob.ar/usuarioQA/asi-234-api-buscador-establecimientos/tree/1.0.0-rc.1


