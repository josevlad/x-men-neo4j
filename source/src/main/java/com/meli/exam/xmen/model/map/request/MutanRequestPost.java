package com.meli.exam.xmen.model.map.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.meli.exam.xmen.model.converter.ListStringToListNitrogenBaseDeserialize;
import com.meli.exam.xmen.model.entity.dna.NitrogenBase;

import java.util.List;

public class MutanRequestPost {

  @JsonDeserialize(using = ListStringToListNitrogenBaseDeserialize.class)
  private List<NitrogenBase> dna;

  public List<NitrogenBase> getDna() {
    return dna;
  }

  public void setDna(List<NitrogenBase> dna) {
    this.dna = dna;
  }
}
