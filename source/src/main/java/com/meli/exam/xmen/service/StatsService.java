package com.meli.exam.xmen.service;

import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcidRepository;
import com.meli.exam.xmen.model.map.request.StatsRequestGet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("statsService")
public class StatsService {

  @Autowired
  private DeoxyribonucleicAcidRepository dnaRepository;

  public StatsRequestGet getStats() {
    Long countHumant = dnaRepository.countByMutantIsFalse();
    Long countMutant = dnaRepository.countByMutantIsTrue();
    return new StatsRequestGet(countMutant, countHumant);
  }
}
