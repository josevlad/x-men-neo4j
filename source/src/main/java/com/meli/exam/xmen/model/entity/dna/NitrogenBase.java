package com.meli.exam.xmen.model.entity.dna;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

@NodeEntity(label = "NB")
public class NitrogenBase {

  @Id @GeneratedValue
  private Long id;

  @Property(name = "value")
  private String value;

  @Property(name = "x")
  private Long x; // equal i

  @Property(name = "y")
  private Long y; // equal j

  public NitrogenBase() {
  }

  public NitrogenBase( String value, Long x, Long y) {
    this.value = value;
    this.y = Long.valueOf(x);
    this.x = Long.valueOf(y);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Long getX() {
    return x;
  }

  public void setX(Long x) {
    this.x = x;
  }

  public Long getY() {
    return y;
  }

  public void setY(Long y) {
    this.y = y;
  }
}
