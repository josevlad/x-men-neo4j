package com.meli.exam.xmen.model.entity.dna;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "dna", path = "dna")
public interface DeoxyribonucleicAcidRepository extends PagingAndSortingRepository<DeoxyribonucleicAcid, Long> {

  Long countByMutantIsTrue();

  Long countByMutantIsFalse();

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(n) WHERE id(dna)={ id } RETURN dna,n")
  Iterable<DeoxyribonucleicAcid> findByIdAndBasesPairs(@Param("id") Long id);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(n:NB),(dna:DNA)-[:BASES_PAIRS]->(h:NB) WHERE " +
           "h.x = n.x+ 1 AND h.y = n.y AND id(dna) = {id} " +
         "CREATE UNIQUE " +
           "(n)-[:HORIZONTAL]->(h) " +
         "RETURN dna")
  DeoxyribonucleicAcid applyHorizontalRelationships(@Param("id") Long id);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(n:NB),(dna:DNA)-[:BASES_PAIRS]->(v:NB) WHERE " +
           "v.x = n.x AND v.y = n.y + 1 AND id(dna) = {id} " +
         "CREATE UNIQUE " +
           "(n)-[:VERTICAL]->(v) " +
         "RETURN dna")
  DeoxyribonucleicAcid applyVerticalRelationships(@Param("id") Long id);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(n:NB),(dna:DNA)-[:BASES_PAIRS]->(r:NB) WHERE " +
           "r.x = n.x + 1 AND r.y = n.y + 1 AND id(dna) = {id} " +
         "CREATE UNIQUE " +
           "(n)-[:RIGHT_OBLIQUE]->(r)" +
         "RETURN dna")
  DeoxyribonucleicAcid applyRightObliqueRelationships(@Param("id") Long id);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(n:NB),(dna:DNA)-[:BASES_PAIRS]->(l:NB) WHERE " +
           "l.x = n.x - 1 AND l.y = n.y + 1 AND id(dna) = {id} " +
         "CREATE UNIQUE " +
           "(n)-[:LEFT_OBLIQUE]->(l)" +
         "RETURN dna")
  DeoxyribonucleicAcid applyLeftObliqueRelationships(@Param("id") Long id);

}
