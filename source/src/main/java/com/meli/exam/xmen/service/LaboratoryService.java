package com.meli.exam.xmen.service;

import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcid;
import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcidRepository;
import com.meli.exam.xmen.model.entity.dna.NitrogenBase;
import com.meli.exam.xmen.model.entity.dna.NitrogenBaseRepository;
import com.meli.exam.xmen.model.map.request.MutanRequestPost;
import com.meli.exam.xmen.service.utils.AsyncTaskServices;
import com.meli.exam.xmen.service.utils.LaboratoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service("laboratoryService")
public class LaboratoryService {

  @Autowired
  private DeoxyribonucleicAcidRepository dnaRepository;

  @Autowired
  private NitrogenBaseRepository nbRepository;

  @Autowired @Qualifier("laboratoryHelper")
  private LaboratoryHelper laboratoryHelper;

  public DeoxyribonucleicAcid analyze(MutanRequestPost mutanRequestPost) {
    DeoxyribonucleicAcid dna = new DeoxyribonucleicAcid();
    dnaRepository.save(dna);
    laboratoryHelper.asyncProcessing(mutanRequestPost.getDna(), dna);
    dna.setMutant(laboratoryHelper.isMutant());
    dnaRepository.save(dna);
    return dna;
  }
}


