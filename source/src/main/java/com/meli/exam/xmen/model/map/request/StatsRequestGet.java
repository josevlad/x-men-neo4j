package com.meli.exam.xmen.model.map.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.text.DecimalFormat;

import static com.sun.javafx.geom.Curve.round;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatsRequestGet {

  @JsonProperty(value = "count_mutant_dna", index = 0)
  private Long countMutantDna;

  @JsonProperty(value = "count_human_dna", index = 1)
  private Long countHumanDna;

  @JsonProperty(value = "ratio", index = 2)
  private Double ratio;

  public StatsRequestGet(Long countMutantDna, Long countHumanDna) {
    this.countMutantDna = countMutantDna;
    this.countHumanDna = countHumanDna;
    buildStats();
  }

  private void buildStats() {
    DecimalFormat df = new DecimalFormat("#.#");
    ratio = Double.valueOf(df.format(Double.valueOf(countMutantDna) / Double.valueOf(countHumanDna)));
  }

  public Long getCountMutantDna() {
    return countMutantDna;
  }

  public void setCountMutantDna(Long countMutantDna) {
    this.countMutantDna = countMutantDna;
  }

  public Long getCountHumanDna() {
    return countHumanDna;
  }

  public void setCountHumanDna(Long countHumanDna) {
    this.countHumanDna = countHumanDna;
  }

  public Double getRatio() {
    return ratio;
  }

  public void setRatio(Double ratio) {
    this.ratio = ratio;
  }
}
