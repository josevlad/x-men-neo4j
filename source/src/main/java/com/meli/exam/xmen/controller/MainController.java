package com.meli.exam.xmen.controller;

import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcid;
import com.meli.exam.xmen.model.map.request.MutanRequestPost;
import com.meli.exam.xmen.model.map.request.StatsRequestGet;
import com.meli.exam.xmen.service.LaboratoryService;
import com.meli.exam.xmen.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api", produces = MediaTypes.HAL_JSON_VALUE)
public class MainController {

  @Autowired @Qualifier("laboratoryService")
  private LaboratoryService laboratoryService;

  @Autowired @Qualifier("statsService")
  private StatsService statsService;

  @PostMapping({"/mutant", "/mutant/"})
  public ResponseEntity analyzeGeneticCode(
          @Valid @RequestBody(required = false) MutanRequestPost mutanRequestPost, Errors errors) throws Exception {
    if (errors.hasErrors()) return ResponseEntity.badRequest().body(errors.hasErrors());
    DeoxyribonucleicAcid analyze = laboratoryService.analyze(mutanRequestPost);
    Resource resource = new Resource<DeoxyribonucleicAcid>(analyze);
    Link link = linkTo(methodOn(DeoxyribonucleicAcidController.class)
            .retrieveAllDnaById(analyze.getId())).withRel("all-dna");
    resource.add(link);
    return ResponseEntity.ok().body(resource);
  }

  @GetMapping({"/stats", "/stats/"})
  public ResponseEntity stats() {
    StatsRequestGet stats = statsService.getStats();
    return ResponseEntity.ok().body(stats);
  }

  @GetMapping({"/matrix/{dim}", "/matrix/{dim}/"})
  public ResponseEntity m(@PathVariable(value = "dim") String dim) {
    List<String> stringList = new ArrayList<String>();
    for (int i = 0; i < Integer.parseInt(dim); i++) {
      String s = "";
      for (int j = 0; j < Integer.parseInt(dim); j++) {
        Random random = new Random();
        int index = random.nextInt("ACGT".length());
        char character = "ACGT".charAt(index);
        s += String.valueOf(character);
      }
      stringList.add(s);
    }
    return ResponseEntity.ok(stringList);
  }
}
