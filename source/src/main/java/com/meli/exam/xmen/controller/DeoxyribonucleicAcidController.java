package com.meli.exam.xmen.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcid;
import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api", produces = MediaTypes.HAL_JSON_VALUE)
public class DeoxyribonucleicAcidController {

  @Autowired
  private DeoxyribonucleicAcidRepository dnaRepository;

  @GetMapping({"/dna", "/dna/"})
  public ResponseEntity<Object> retrieveAllNodeDna() {
    Iterable<DeoxyribonucleicAcid> all = dnaRepository.findAll();
    List<Resource<DeoxyribonucleicAcid>> resourceList = new ArrayList<>();
    for (DeoxyribonucleicAcid dna : all) {
      Resource resource = new Resource<DeoxyribonucleicAcid>(dna);
      Link link = linkTo(methodOn(DeoxyribonucleicAcidController.class)
              .retrieveAllDnaById(dna.getId())).withRel("self");
      resource.add(link);
      resourceList.add(resource);
    }
    return ResponseEntity.ok(resourceList);
  }

  @GetMapping({"/dna/{id}", "/dna/{id}/"})
  public ResponseEntity<Object> retrieveAllDnaById(@PathVariable("id") long id) {
    Optional<DeoxyribonucleicAcid> dna = dnaRepository.findById(id);
    Resource resource = new Resource<DeoxyribonucleicAcid>(dna.get());
    Link link = linkTo(methodOn(DeoxyribonucleicAcidController.class)
            .retrieveAllNodeDna()).withRel("all-dna");
    resource.add(link);
    return ResponseEntity.ok(resource);
  }

  @GetMapping({"/dna/{id}/nodes", "/dna/{id}/nodes/"})
  public ResponseEntity<Object> retrieveAllDnaByIdWithNodes(@PathVariable("id") long id) {
    Iterable<DeoxyribonucleicAcid> byIdAndBasesPairs = dnaRepository.findByIdAndBasesPairs(id);
    return ResponseEntity.ok(byIdAndBasesPairs);
  }
}
