package com.meli.exam.xmen.service.utils;

import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcid;
import com.meli.exam.xmen.model.entity.dna.NitrogenBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service("laboratoryHelper")
public class LaboratoryHelper {

  @Autowired @Qualifier("asyncTaskServices")
  private AsyncTaskServices asyncTaskServices;

  public void asyncProcessing(List<NitrogenBase> nbList, DeoxyribonucleicAcid dna) {
    int n = nbList.size();
    if (n >= 1 && n < 100) oneThread(nbList, dna);
    if (n >= 100 && n < 900) threeThread(split(nbList, 3), dna);
    if (n >= 900 && n < 10000) fiveThread(split(nbList, 5), dna);
    if (n >= 10000) fiveThread(split(nbList, 7), dna);
  }

  public Boolean isMutant() {
    return (asyncTaskServices.getIsMutant().endsWith("MUTANT")) ? Boolean.TRUE : Boolean.FALSE;
  }

  private void oneThread(List<NitrogenBase> nbList, DeoxyribonucleicAcid dna) {
    CompletableFuture<Boolean> processA = asyncTaskServices.saveSegmentMatrix(nbList, dna, Boolean.TRUE);
    CompletableFuture.allOf(processA).join();
  }

  private void threeThread(List<List<NitrogenBase>> nbSmallerLists, DeoxyribonucleicAcid dna) {
    CompletableFuture<Boolean> processOne = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(0), dna);
    CompletableFuture<Boolean> processTwo = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(1), dna);
    CompletableFuture<Boolean> processThree = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(2), dna, Boolean.TRUE);
    CompletableFuture.allOf(processOne, processTwo, processThree).join();
  }

  private void fiveThread(List<List<NitrogenBase>> nbSmallerLists, DeoxyribonucleicAcid dna) {
    CompletableFuture<Boolean> processOne = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(0), dna);
    CompletableFuture<Boolean> processTwo = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(1), dna);
    CompletableFuture<Boolean> processThree = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(2), dna);
    CompletableFuture<Boolean> processFour = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(3), dna);
    CompletableFuture<Boolean> processFive = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(4), dna, Boolean.TRUE);
    CompletableFuture.allOf(processOne, processTwo, processThree, processFour, processFive).join();
  }

  private void sevenThread(List<List<NitrogenBase>> nbSmallerLists, DeoxyribonucleicAcid dna) {
    CompletableFuture<Boolean> processOne = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(0), dna);
    CompletableFuture<Boolean> processTwo = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(1), dna);
    CompletableFuture<Boolean> processThree = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(2), dna);
    CompletableFuture<Boolean> processFour = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(3), dna);
    CompletableFuture<Boolean> processFive = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(4), dna);
    CompletableFuture<Boolean> processSix = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(5), dna);
    CompletableFuture<Boolean> processSeven = asyncTaskServices.saveSegmentMatrix(nbSmallerLists.get(6), dna, Boolean.TRUE);
    CompletableFuture.allOf(processOne, processTwo, processThree, processFour, processFive, processSix, processSeven).join();
  }

  private <T> List<List<T>> split(List<T> list, final int part) {
    List<List<T>> segmentedList = new ArrayList<List<T>>();
    int div = list.size() / part;
    int mod = list.size() % part;
    int n = list.size();
    for (int i = 0; i < n; i += div) {
      segmentedList.add(new ArrayList<T>(list.subList(i, Math.min(n, i + div))));
      if (i >= n && mod > 0)
        segmentedList.add(new ArrayList<T>(list.subList(Math.min(n, i + div), n)));
    }
    return segmentedList;
  }
}
