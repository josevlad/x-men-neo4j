package com.meli.exam.xmen.service.utils;

import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcid;
import com.meli.exam.xmen.model.entity.dna.DeoxyribonucleicAcidRepository;
import com.meli.exam.xmen.model.entity.dna.NitrogenBase;
import com.meli.exam.xmen.model.entity.dna.NitrogenBaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service("asyncTaskServices")
public class AsyncTaskServices {

  private String isMutant = "next";

  @Autowired
  private NitrogenBaseRepository nbRepository;

  @Autowired
  private DeoxyribonucleicAcidRepository dnaRepository;

  @Async
  @Transactional
  public CompletableFuture<Boolean> saveSegmentMatrix(List<NitrogenBase> nbListSegment, DeoxyribonucleicAcid dna) {
    dna.setBasesPairs(nbListSegment);
    dnaRepository.save(dna);
    return CompletableFuture.completedFuture(Boolean.TRUE);
  }

  @Async @Transactional
  public CompletableFuture<Boolean> saveSegmentMatrix(List<NitrogenBase> nbListSegment, DeoxyribonucleicAcid dna, Boolean rel) {
    isMutant = "next";
    dna.setBasesPairs(nbListSegment);
    dnaRepository.save(dna);

    dnaRepository.applyHorizontalRelationships(dna.getId());
    int concurrences = findConcurrences(0, dna.getId());

    if (concurrences >= 2) {
      isMutant = "MUTANT";
      buildRelationshipsBetweenNB(0, dna.getId(), Boolean.TRUE);
      CompletableFuture.completedFuture(Boolean.TRUE);
    } else {
      applyRelationshipsAndSearchConcordances(dna.getId(), isMutant, 0);
    }
    return CompletableFuture.completedFuture(Boolean.TRUE);
  }

  public void applyRelationshipsAndSearchConcordances(Long id, String result, Integer i) {
    if (result.equals("next")) {
      if (i <= 3) {
        buildRelationshipsBetweenNB(i + 1, id, Boolean.FALSE);
        int concurrences = findConcurrences(i + 1, id);
        if (concurrences >= 2) {
          this.isMutant = "MUTANT";
          buildRelationshipsBetweenNB(i + 1, id, Boolean.TRUE);
        }
      } else isMutant = "HUMAN";
      applyRelationshipsAndSearchConcordances(id, isMutant, i + 1);
    } else  isMutant = "HUMAN";
  }

  @Async @Transactional
  public void buildRelationshipsBetweenNB(Integer level, Long id, Boolean async) {
    switch (level) {
      case 0: break;
      case 1: dnaRepository.applyRightObliqueRelationships(id);
        break;
      case 2: dnaRepository.applyVerticalRelationships(id);
        break;
      case 3: dnaRepository.applyLeftObliqueRelationships(id);
        break;
    }
    if (level >= 3 && async) buildRelationshipsBetweenNB(level + 1, id, Boolean.TRUE);
  }

  public int findConcurrences(Integer level, Long id) {
    int result = 0;
    switch (level) {
      case 0: result = Math.toIntExact(nbRepository.lookForHorizontalConcurrences(id));
        break;
      case 1: result = Math.toIntExact(nbRepository.lookForRightObliqueConcurrences(id));
        break;
      case 2: result = Math.toIntExact(nbRepository.lookForVertivalConcurrences(id));
        break;
      case 3: result = Math.toIntExact(nbRepository.lookForLeftObliqueConcurrences(id));
        break;
    }
    return result;
  }

  public String getIsMutant() {
    return isMutant;
  }

  public void setIsMutant(String isMutant) {
    this.isMutant = isMutant;
  }
}
