package com.meli.exam.xmen.model.entity.dna;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collection;

@RepositoryRestResource(collectionResourceRel = "nb", path = "nb")
public interface NitrogenBaseRepository extends PagingAndSortingRepository<NitrogenBase, Long> {

  @Query("MATCH (dna:DNA) WHERE id(dna)= { id } " +
          "CREATE " +
          "(n:NB {value:{ node }.value, x:{ node }.x, y:{ node }.y})," +
          "(dna)-[:BASES_PAIRS]->(n) " +
          "RETURN n")
  NitrogenBase saveNodeWithRelationshipDNA(@Param("id") Long id, @Param("node") NitrogenBase node);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(a)-[:HORIZONTAL]->(b)-[:HORIZONTAL]->(c)-[:HORIZONTAL]->(d) " +
          "WHERE a.value = b.value = c.value = d.value AND id(dna) = { id } " +
          "RETURN count(*)")
  Long lookForHorizontalConcurrences(@Param("id") Long id);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(a)-[:RIGHT_OBLIQUE]->(b)-[:RIGHT_OBLIQUE]->(c)-[:RIGHT_OBLIQUE]->(d) " +
          "WHERE a.value = b.value = c.value = d.value AND id(dna) = { id } " +
          "RETURN count(*)")
  Long lookForRightObliqueConcurrences(@Param("id") Long id);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(a:NB)-[:VERTICAL]->(b:NB)-[:VERTICAL]->(c:NB)-[:VERTICAL]->(d:NB) " +
          "WHERE a.value = b.value = c.value = d.value AND id(dna) = { id } " +
          "RETURN count(*)")
  Long lookForVertivalConcurrences(@Param("id") Long id);

  @Query("MATCH (dna:DNA)-[:BASES_PAIRS]->(a)-[:LEFT_OBLIQUE]->(b)-[:LEFT_OBLIQUE]->(c)-[:LEFT_OBLIQUE]->(d) " +
          "WHERE a.value = b.value = c.value = d.value AND id(dna) = { id } " +
          "RETURN count(*)")
  Long lookForLeftObliqueConcurrences(@Param("id") Long id);


}
