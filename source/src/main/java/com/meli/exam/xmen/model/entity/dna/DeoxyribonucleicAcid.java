package com.meli.exam.xmen.model.entity.dna;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.neo4j.ogm.annotation.*;

import java.util.ArrayList;
import java.util.List;

@NodeEntity(label = "DNA")
public class DeoxyribonucleicAcid  {

  @Id @GeneratedValue
  private Long id;

  @Property(name = "mutant")
  private Boolean isMutant;

  @Relationship(type = "BASES_PAIRS", direction = Relationship.OUTGOING)
  private List<NitrogenBase> basesPairs = new ArrayList<>();

  public DeoxyribonucleicAcid() {
  }

  public DeoxyribonucleicAcid(List<NitrogenBase> basesPairs) {
    this.basesPairs = basesPairs;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Boolean getMutant() {
    return isMutant;
  }

  public void setMutant(Boolean mutant) {
    isMutant = mutant;
  }

  public List<NitrogenBase> getBasesPairs() {
    return basesPairs;
  }

  public void setBasesPairs(List<NitrogenBase> basesPairs) {
    this.basesPairs = basesPairs;
  }
}
