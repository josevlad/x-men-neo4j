package com.meli.exam.xmen.model.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.meli.exam.xmen.model.entity.dna.NitrogenBase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class ListStringToListNitrogenBaseDeserialize extends JsonDeserializer<List<NitrogenBase>> {

  private List<List<String>> sample = new ArrayList<List<String>>();
  private List<NitrogenBase> nbList = new ArrayList<NitrogenBase>();

  @Override
  public List<NitrogenBase> deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
    sample.clear();
    JsonNode node = jp.getCodec().readTree(jp);
    init: for (JsonNode str : node) {
      if (node.size() != str.asText().length())
        throw dc.instantiationException(String.class, "The dimensions of the DNA sample are not correct, they must be N x N");
      Pattern pattern = Pattern.compile("^[ACGT]+$");
      if(!pattern.matcher(str.asText()).matches())
        throw dc.instantiationException(String.class, "Values not allowed in the DNA sample, only [A C G T]");
      sample.add(Arrays.asList(str.asText().split("")));
    }
    return buildNitrogenBaseList(sample);
  }

  public void clear() {
    sample.clear();
  }

  public List<NitrogenBase> buildNitrogenBaseList(List<List<String>> list) {
    nbList.clear();
    for (int i = 0; i < list.size(); i++) {
      for (int j = 0; j < list.get(i).size(); j++) {
        NitrogenBase nitrogenBase = new NitrogenBase(list.get(i).get(j).toString(), Long.valueOf(i), Long.valueOf(j));
        nbList.add(nitrogenBase);
      }
    }
    clear();
    return nbList;
  }
}
