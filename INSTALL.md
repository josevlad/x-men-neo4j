# INSTALL
Toda la descripcion del contenido de esta instalación se encuentra documentado en este 
[archivo](http://git-asi.buenosaires.gob.ar/usuarioQA/asi-234-api-buscador-establecimientos/blob/master/CHANGELOG.md).

El formato se basa en [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

Este proyecto se adhiere a [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

### Requerimientos del server ####

- __[Neo4j](https://neo4j.com/download/other-releases/#releases)__: 3.3.3
- __[java](https://www.java.com/es/download/help/index_installing.xml)__: 8  
- __[apache2](http://httpd.apache.org/docs/2.4/install.html)__  

### Commando de actualizacion

Clonado del proyecto

    git clone 

El proceso de actualizacion se ejecuta con un que dispara un jar:

	java -jar target/x-men-neo4j-v1.0.0-rc.1.jar